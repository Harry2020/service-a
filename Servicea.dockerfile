FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive

# Install git and wget
RUN apt-get update
RUN apt-get install -y git wget

#Install go with specific version
RUN cd /home  && \
    wget https://studygolang.com/dl/golang/go1.17.6.linux-amd64.tar.gz && \
    tar -xzvf go1.17.6.linux-amd64.tar.gz && \
    mv go /usr/local/ && \
    chmod -R 777 /usr/local/go && \
    ln -sf /usr/local/go/bin/go /usr/local/bin/go && \
    mkdir GOPATH && \
    chmod -R 777 GOPATH && \
    echo 'export GOROOT=/usr/local/go' >> ~/.bashrc  && \
    echo 'export GOPATH=/home/GOPATH' >> ~/.bashrc   && \
    echo 'export PATH=$GOPATH/bin:$GOROOT/bin:$PATH' >> ~/.bashrc  && \
    /bin/bash -c "source ~/.bashrc"

#Clone Code and Compile
RUN cd /home  && \
    git clone https://gitlab.com/shreychen/service-a.git  && \
    export GOPROXY=https://goproxy.cn  && \
    cd service-a/  && \
    go mod tidy  && \
    go build

#Clear Cache
RUN apt-get autoclean && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /home/service-a

#Start Service A
CMD ["bash","-c","./service-a --file config.yaml"]