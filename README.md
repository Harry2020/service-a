Get the application URL by running these commands:

export POD_NAME=$(kubectl get pods --namespace service-zhonghuaqiang -l "app.kubernetes.io/name=serviceachart,app.kubernetes.io/instance=harrytest" -o jsonpath="{.items[0].metadata.name}")

export CONTAINER_PORT=$(kubectl get pod --namespace service-zhonghuaqiang $POD_NAME -o jsonpath="{.spec.containers[0].ports[0].containerPort}")

echo "Visit http://127.0.0.1:8080 to use your application"

kubectl --namespace service-zhonghuaqiang port-forward $POD_NAME 8080:$CONTAINER_PORT


http://127.0.0.1:8080/healthz

http://127.0.0.1:8080/api/v1/features
